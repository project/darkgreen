<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>

  <title><?php print $head_title ?></title>
  <meta name="AUTHOR" content="http://www.finex.org" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="KEYWORDS" content="KEYWORDS" />

  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>

  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

</head>

<body>

<div id="header">
  <div id="searchbar" >
    <?php print $search_box ?>
  </div>
</div>

<div id="mission">
  <?php print $mission ?>
</div>



<table id="content">
  <tr>
    <?php if ($sidebar_left) { ?>
      <td id="sidebar-left">
        <?php print $sidebar_left ?>
      </td>
    <?php } ?>

    <td valign="top" >
      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
    </td>
  </tr>
</table>


<table id="footer">
  <tr>
    <td>
      <?php print $footer_message ?>

      <?php print $feed_icons ?>
  
      <div id="author" > <!-- PLEASE DON'T REMOVE THIS SECTION -->
        Elegant template by <a href="http://www.finex.org">FiNe<strong>X</strong><em>design</em></a>
        <a href="http://www.finex.org">
          <img src="<?php print base_path() . $directory ?>/img/finex_design.gif" alt="FiNeXdesign" style="border:0"/>
        </a>
      </div>
    </td>
  </tr>
</table>


<?php print $closure ?>
</body>
</html>
